

--Script to create relationship between table superhero and table assistant

USE SuperHeroesDb;


ALTER TABLE assistant ADD FK_superhero_id INTEGER NOT NULL UNIQUE,
FOREIGN KEY(FK_superhero_id) REFERENCES superhero(superhero_id);