

--Script to create relationship between Superhero and Power

USE SuperHeroesDb;

CREATE TABLE superhero_power(
	superhero_id INTEGER,
	power_id INTEGER,
	PRIMARY KEY (superhero_id, power_id),
	FOREIGN KEY (superhero_id) REFERENCES superhero(superhero_id),
	FOREIGN KEY (power_id) REFERENCES power(power_id));