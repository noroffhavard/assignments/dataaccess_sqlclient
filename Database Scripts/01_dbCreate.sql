

--Script to create a database

USE tempdb;
GO
DECLARE @SQL nvarchar(1000);
IF EXISTS (SELECT 1 FROM sys.databases WHERE [name] = N'SuperheroesDb')
BEGIN
	SET @SQL = N'USE [SuperheroesDb];
				
				ALTER DATABASE SuperheroesDb SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
				USE [tempdb]

				DROP DATABASE SuperheroesDb;';
		EXEC (@SQL);
	END

GO

CREATE DATABASE SuperHeroesDb;
GO