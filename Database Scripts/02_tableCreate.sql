

--Script to create 3 tables in SuperHero

USE SuperheroesDb;

CREATE TABLE superhero
(superhero_id INT NOT NULL IDENTITY PRIMARY KEY,
	name NVARCHAR(50) NOT NULL,
	alias NVARCHAR(50),
	origin NVARCHAR(50));

CREATE TABLE assistant
(assistant_id INT NOT NULL IDENTITY PRIMARY KEY,
	name NVARCHAR(50) NOT NULL);

CREATE TABLE power
(power_id INT NOT NULL IDENTITY PRIMARY KEY,
	name NVARCHAR(50) NOT NULL,
	description NVARCHAR(150));