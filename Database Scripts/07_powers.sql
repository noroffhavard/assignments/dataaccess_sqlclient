

--Script to populate table power and table superhero_power

USE SuperHeroesDb;

INSERT INTO power VALUES
('Fly', 'This power makes the superhero able to fly in the air'),
('The Force', 'This power gives the superhero the ability to use the force'),
('Banana', 'This power gives the superhero the ability to shoot bananas from their wrist'),
('Fireball', 'This power gives the superhero the ability to drink a whole bottle of fireball');

INSERT INTO superhero_power VALUES
(1, 1),
(1, 2),
(2, 3),
(3, 1);