

--Script to populate table superhero

USE SuperHeroesDb;

INSERT INTO superhero VALUES
('Obi Wan Kenobi', 'Ben', 'Tatooine'),
('Fredrik', 'Monkeyman', 'Earth'),
('Kal-El', 'Superman','Krypton');