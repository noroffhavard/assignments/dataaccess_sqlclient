﻿using CSharp_DataAccess_With_SQLClient.Models;
using CSharp_DataAccess_With_SQLClient.Repository;

namespace CSharp_DataAccess_With_SQLClient
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ICustomerRepository repository = new CustomerRepository();
            ICustomerCountryRepository customerCountryRepository = new CustomerCountryRepository();
            ICustomerSpenderRepository customerSpenderRepository = new CustomerSpenderRepository();
            ICustomerGenreRepository customerGenreRepository= new CustomerGenreRepository();

            //TestPrintAllCustomerCountry(customerCountryRepository);
            //TestPrintAllCustomerExpenses(customerSpenderRepository);
            TestPrintCustomerGenre(customerGenreRepository);
            
        }

        static void PrintAllCustomers(List<Customer> customers)
        {
            foreach (Customer customer in customers)
            {
                PrintCustomer(customer);
            }
        }

        static void PrintCustomer(Customer customer)
        {
            Console.WriteLine($"{customer.CustomerId} {customer.FirstName} {customer.LastName} {customer.Country} {customer.PostalCode} {customer.Phone} {customer.Email}");
        }

        static void PrintPageOfCustomers(List<Customer> customers)
        {
            foreach (Customer customer in customers)
            {
                PrintCustomer(customer);
            }
        }

        static void AddCustomer(ICustomerRepository repository)
        {
            Customer test = new Customer()
            {
                FirstName = "",
                LastName = "",
                Country = "",
                PostalCode = "",
                Phone = "",
                Email = ""
            };
            if (repository.AddCustomer(test))
            {
                Console.WriteLine("New customer added");
            }
            else
            {
                Console.WriteLine("Customer could not be added");
            }
        }

        static void TestPrintAllRecords(ICustomerRepository repository)
        {
            PrintAllCustomers(repository.GetAllCustomers());
        }

        static void TestPrintOneRecordById(ICustomerRepository repository)
        {
            PrintCustomer(repository.GetCustomerById(1));
        }

        static void TestPrintOneRecordByName(ICustomerRepository repository)
        {
            PrintCustomer(repository.GetCustomerByName("John"));
        }

        static void TestPrintPageOfCustomers(ICustomerRepository repository)
        {
            PrintPageOfCustomers(repository.GetPageOfCustomer(10, 10));
        }

        static void TestAddCustomer(ICustomerRepository repository)
        {
            Customer test = new Customer()
            {
                FirstName = "Dravåh",
                LastName = "Dnaldam",
                Country = "Norway",
                PostalCode = "32412",
                Phone = "93294239",
                Email = "ljdsfns@gmail.com"
            };
            if(repository.AddCustomer(test))
            {
                Console.WriteLine("New customer added");
                PrintCustomer(repository.GetCustomerByName(test.FirstName));
            }
            else
            {
                Console.WriteLine("Customer could not be added");
            }
        }

        static void TestUpdateCustomer(ICustomerRepository repository, int customerToUpdate)
        {
            Customer test = new Customer()
            {
                CustomerId = customerToUpdate,
                FirstName = "Dravåh",
                LastName = "Dnaldam",
                Country = "Norway",
                PostalCode = "32412",
                Phone = "93294239",
                Email = "ljdsfns@gmail.com"
            };
            if(repository.UpdateCustomer(test))
            {
                Console.WriteLine("Customer updated");
                PrintCustomer(repository.GetCustomerById(test.CustomerId));
            }
            else
            {
                Console.WriteLine("Customer could not be updated");
            }
        }

        /// <summary>
        /// Helper method to print all CustomerCountry entities.
        /// </summary>
        /// <param name="customerCountry">CustomerCountry object</param>
        static void PrintCustomerCountry(CustomerCountry customerCountry)
        {
            Console.WriteLine($"{customerCountry.Country} {customerCountry.Count}");
        }
        static void PrintAllCustomerCountry(List<CustomerCountry> customerCountry)
        {
            foreach (CustomerCountry item in customerCountry)
            {
                PrintCustomerCountry(item);
            }
        }
        static void TestPrintAllCustomerCountry(ICustomerCountryRepository customerCountryRepository)
        {
            PrintAllCustomerCountry(customerCountryRepository.GetCountOfCustomersInCountry());
        }

        static void PrintCustomerSpender(CustomerSpender customerSpender)
        {
            Console.WriteLine($"{customerSpender.CustomerId} {customerSpender.FirstName} " +
                $"{customerSpender.LastName} {customerSpender.Total}");
        }

        static void PrintAllCustomerSpender(List<CustomerSpender> customerSpender)
        {
            foreach (CustomerSpender item in customerSpender)
            {
                PrintCustomerSpender(item);
            }
        }

        static void TestPrintAllCustomerExpenses(ICustomerSpenderRepository customerSpenderRepository)
        {
            PrintAllCustomerSpender(customerSpenderRepository.GetListOfCustomersExpenses());
        }

        static void PrintCustomerGenre(CustomerGenre customerGenre)
        {
            Console.WriteLine($"{customerGenre.CustomerId} {customerGenre.MostPopularGenre} {customerGenre.TrackCount}");
        }
        static void PrintCustomerGenre2(List<CustomerGenre> customerGenre)
        {
            foreach(CustomerGenre item in customerGenre)
            {
                PrintCustomerGenre(item);
            }
        }
        static void TestPrintCustomerGenre(ICustomerGenreRepository customerGenreRepository)
        {
            PrintCustomerGenre2(customerGenreRepository.GetCustomerGenres(9));
        }
    }
}