﻿using CSharp_DataAccess_With_SQLClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_DataAccess_With_SQLClient.Repository
{
    public interface ICustomerRepository
    {
        public List<Customer> GetAllCustomers();
        public Customer GetCustomerById(int id);
        public Customer GetCustomerByName(string name);
        public List<Customer> GetPageOfCustomer(int offset, int fetchNextRows);
        public bool AddCustomer(Customer customer);
        public bool UpdateCustomer(Customer customer);
    }
}