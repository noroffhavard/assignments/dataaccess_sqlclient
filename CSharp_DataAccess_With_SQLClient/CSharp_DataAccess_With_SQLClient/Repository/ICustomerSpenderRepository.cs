﻿using CSharp_DataAccess_With_SQLClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_DataAccess_With_SQLClient.Repository
{
    public interface ICustomerSpenderRepository
    {
        public List<CustomerSpender> GetListOfCustomersExpenses();
    }
}
