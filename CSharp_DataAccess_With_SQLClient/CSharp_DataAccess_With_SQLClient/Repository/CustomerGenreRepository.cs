﻿using CSharp_DataAccess_With_SQLClient.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CSharp_DataAccess_With_SQLClient.Repository
{
    public class CustomerGenreRepository : ICustomerGenreRepository
    {
        public List<CustomerGenre> GetCustomerGenres(int id)
        {
            List<CustomerGenre> customerGenreList = new List<CustomerGenre>();
            string sql = "SELECT TOP 1 WITH TIES Customer.CustomerId, Genre.Name AS MostPopularGenre, COUNT(Genre.GenreId) " +
            "AS TrackCount FROM Customer INNER JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId INNER JOIN InvoiceLine " +
            "ON Invoice.InvoiceId = InvoiceLine.InvoiceId INNER JOIN Track ON InvoiceLine.TrackId = Track.TrackId INNER JOIN Genre " +
            "ON Track.GenreId = Genre.GenreId WHERE Customer.CustomerId = @id GROUP BY Customer.CustomerId, Genre.Name ORDER BY TrackCount";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@id", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerGenre temp = new CustomerGenre();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.MostPopularGenre = reader.GetString(1);
                                temp.TrackCount = reader.GetInt32(2);
                                customerGenreList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message);
            }
            return customerGenreList;
        }
    }
}