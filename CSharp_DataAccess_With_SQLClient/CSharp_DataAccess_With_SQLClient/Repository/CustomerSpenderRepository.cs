﻿using CSharp_DataAccess_With_SQLClient.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_DataAccess_With_SQLClient.Repository
{
    public class CustomerSpenderRepository : ICustomerSpenderRepository
    {
        public List<CustomerSpender> GetListOfCustomersExpenses()
        {
            List<CustomerSpender> customerSpenderList = new List<CustomerSpender>();
            string sql = "SELECT Customer.CustomerId, Customer.FirstName, Customer.LastName, SUM(Invoice.Total) AS Total " +
                "FROM Customer LEFT JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId GROUP BY Customer.CustomerId, " +
                "Customer.FirstName, Customer.LastName ORDER BY [Total] DESC";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerSpender temp = new CustomerSpender();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.IsDBNull(1) ? "" : reader.GetString(1);
                                temp.LastName = reader.IsDBNull(2) ? "" : reader.GetString(2);
                                temp.Total = reader.IsDBNull(3) ? 0 : reader.GetDecimal(3);
                                customerSpenderList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message);
            }
            return customerSpenderList;
        }
    }
}