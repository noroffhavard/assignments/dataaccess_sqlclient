﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_DataAccess_With_SQLClient.Repository
{
    public class ConnectionHelper
    {
        public static string GetConnectionString()
        {
            SqlConnectionStringBuilder connectStringBuilder = new SqlConnectionStringBuilder();
            connectStringBuilder.DataSource = "N-NO-01-03-5424\\SQLEXPRESS";
            connectStringBuilder.InitialCatalog = "Chinook";
            connectStringBuilder.IntegratedSecurity= true;
            return connectStringBuilder.ConnectionString;
        }
    }
}