﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_DataAccess_With_SQLClient.Models
{
    public class CustomerCountry
    {
        public string Country { get; set; }
        public int Count { get; set; }
    }
}
