﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_DataAccess_With_SQLClient.Models
{
    public class CustomerGenre
    {
        public int CustomerId { get; set; }
        public string MostPopularGenre { get; set; }
        public int TrackCount { get; set; }
    }
}